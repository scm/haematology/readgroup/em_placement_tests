# EM_placement_tests

This repository hosts the test cases for the new cryo-em docking program developed in the Phaser team, voyager.em_placement

## Structure of the repository

There are three main subfolders: one (temporarily) containing the data (full and half maps for the cryo-em structures), one containing the models (deposited, homologues and predicted) that can be used for docking, and finally, one containing the phil configuration files needed to run and some results of running the phenix.voyager.em_placement program.

Inside each of those subfolders there will be folders with the names of the structure that will contain the relevant files.
