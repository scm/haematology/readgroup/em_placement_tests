#######################
# Respiratory complex #
#######################

wget https://files.wwpdb.org/pub/emdb/structures/EMD-12654/map/emd_12654.map.gz;
gunzip emd_12654.map.gz;
wget https://files.wwpdb.org/pub/emdb/structures/EMD-12654/other/emd_12654_half_map_2.map.gz;
gunzip emd_12654_half_map_2.map.gz;
wget https://files.wwpdb.org/pub/emdb/structures/EMD-12654/other/emd_12654_half_map_1.map.gz;
gunzip emd_12654_half_map_1.map.gz;

########
# MutS #
########

# Fetch data
wget https://files.wwpdb.org/pub/emdb/structures/EMD-11792/map/emd_11792.map.gz;
gunzip emd_11792.map.gz;
wget https://files.wwpdb.org/pub/emdb/structures/EMD-11792/other/emd_11792_half_map_1.map.gz;
gunzip emd_11792_half_map_1.map.gz;
wget https://files.wwpdb.org/pub/emdb/structures/EMD-11792/other/emd_11792_half_map_2.map.gz;
gunzip emd_11792_half_map_2.map.gz;

#########################
# CFTR deltaF508 mutant #
#########################

wget https://files.wwpdb.org/pub/emdb/structures/EMD-28172/map/emd_28172.map.gz;
gunzip emd_28172.map.gz;
wget https://files.wwpdb.org/pub/emdb/structures/EMD-28172/other/emd_28172_half_map_2.map.gz;
gunzip emd_28172_half_map_2.map.gz;
wget https://files.wwpdb.org/pub/emdb/structures/EMD-28172/other/emd_28172_half_map_1.map.gz;
gunzip emd_28172_half_map_1.map.gz;
