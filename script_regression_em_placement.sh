#################
# GABA receptor #
#################

# Fetch data
cd ./data/GABA_emd_11657/ ;
wget https://files.wwpdb.org/pub/emdb/structures/EMD-11657/map/emd_11657.map.gz;
gunzip emd_11657.map.gz;
wget https://files.wwpdb.org/pub/emdb/structures/EMD-11657/other/emd_11657_half_map_1.map.gz;
gunzip emd_11657_half_map_1.map.gz;
wget https://files.wwpdb.org/pub/emdb/structures/EMD-11657/other/emd_11657_half_map_2.map.gz;
gunzip emd_11657_half_map_2.map.gz;

# Test with crystal structure of membrane domain
echo "Starting GABA receptor membrane domain"
cd ../../runs/GABA_emd_11657/test_membrane/ ;
rm -rf emplaced_files_* ; # Clean up before new run
phenix.voyager.em_placement em_place_4cofA_membrane.phil > em_placement.log ;
rm voyager_strategy.log ;

# Test with AF model of megabody component
echo "Starting GABA receptor megabody"
cd ../test_megabody/ ;
rm -rf emplaced_files_* ; # Clean up before new run
phenix.voyager.em_placement em_place_AF_megabody.phil > em_placement.log ;
rm voyager_strategy.log ;

# Remove data
cd ../../../data/GABA_emd_11657/ ;
rm *.map* ;
cd ../.. ;

###########
# Betagal #
###########

# Fetch data
# For some reason, this entry doesn't have .gz and uses a different naming convention for other.
cd ./data/betagal_emd_2984/ ;
wget https://files.wwpdb.org/pub/emdb/structures/EMD-2984/map/emd_2984.map.gz;
gunzip emd_2984.map.gz ;
wget https://files.wwpdb.org/pub/emdb/structures/EMD-2984/other/EMD-2984-half-1.map;
mv EMD-2984-half-1.map emd_2984_half_map_1.map ;
wget https://files.wwpdb.org/pub/emdb/structures/EMD-2984/other/EMD-2984-half-2.map;
mv EMD-2984-half-2.map emd_2984_half_map_2.map ;

# First test with perfect monomer model
echo "Starting betagal perfect monomer"
cd ../../runs/betagal_emd_2984/test_perfect_monomer/ ;
rm -rf emplaced_files_* ; # Clean up before new run
phenix.voyager.em_placement emplace_chainA.phil > em_placement.log ;
rm voyager_strategy.log ;

# Second test with perfect beta-barrel domain model
echo "Starting betagal beta-barrel domain"
cd ../test_perfect_BBdomain/ ;
rm -rf emplaced_files_* ; # Clean up before new run
phenix.voyager.em_placement emplace_BBdomain.phil > em_placement.log ;
rm voyager_strategy.log ;

# Third test with homolog model
echo "Starting betagal homologue monomer"
cd ../test_homolog/ ;
rm -rf emplaced_files_* ; # Clean up before new run
phenix.voyager.em_placement test_homolog_fullmap.phil > em_placement.log ;
rm voyager_strategy.log ;

# Remove data
cd ../../../data/betagal_emd_2984/ ;
rm *.map* ;
cd ../.. ;

###############
# Apoferritin #
###############

# Fetch data
cd ./data/apoferritin_emd_6714/ ;
wget https://files.wwpdb.org/pub/emdb/structures/EMD-6714/map/emd_6714.map.gz;
gunzip emd_6714.map.gz;
wget https://files.wwpdb.org/pub/emdb/structures/EMD-6714/other/emd_6714_half_map_1.map.gz;
gunzip emd_6714_half_map_1.map.gz;
wget https://files.wwpdb.org/pub/emdb/structures/EMD-6714/other/emd_6714_half_map_2.map.gz;
gunzip emd_6714_half_map_2.map.gz;

# First test, perfect model
echo "Starting apoferritin perfect monomer"
cd ../../runs/apoferritin_emd_6714/perfect_model_fullmap/ ;
rm -rf emplaced_files_* ; # Clean up before new run
phenix.voyager.em_placement test_apo_fullmap_perfect.phil > em_placement.log ;
rm voyager_strategy.log ;

# Second test, homolog model
echo "Starting apoferritin homologue monomer"
cd ../homolog_model/ ;
rm -rf emplaced_files_* ; # Clean up before new run
phenix.voyager.em_placement emplace_2cei_deleteE.phil > em_placement.log ;
rm voyager_strategy.log ;

# Remove data
cd ../../../data/apoferritin_emd_6714/ ;
rm *.map* ;
cd ../.. ;

#######################
# Respiratory complex #
#######################

# Fetch data
cd ./data/respiratory_complex_emd_12654/ ;
wget https://files.wwpdb.org/pub/emdb/structures/EMD-12654/map/emd_12654.map.gz;
gunzip emd_12654.map.gz;
wget https://files.wwpdb.org/pub/emdb/structures/EMD-12654/other/emd_12654_half_map_2.map.gz;
gunzip emd_12654_half_map_2.map.gz;
wget https://files.wwpdb.org/pub/emdb/structures/EMD-12654/other/emd_12654_half_map_1.map.gz;
gunzip emd_12654_half_map_1.map.gz;

# Use perfect model for one of the chains
echo "Starting respiratory complex perfect chain M"
cd ../../runs/respiratory_complex_emd_12654/perfect_model/ ;
rm -rf emplaced_files_* ; # Clean up before new run
phenix.voyager.em_placement em_place_chainM.phil > em_placement.log ;
rm voyager_strategy.log ;

# Use homolog model for the membrane domains
echo "Starting respiratory complex membrane domain"
cd ../homolog_model_membrane/ ;
rm -rf emplaced_files_* ; # Clean up before new run
phenix.voyager.em_placement em_place_membrane_homolog.phil > em_placement.log ;
rm voyager_strategy.log ;

# Three runs with homolog models for three chains, decreasingly well-ordered
echo "Starting respiratory complex homologue chain N"
cd ../homolog_model_chainN/ ;
rm -rf emplaced_files_* ; # Clean up before new run
phenix.voyager.em_placement em_place_chainNhomolog.phil > em_placement.log ;
rm voyager_strategy.log ;

echo "Starting respiratory complex homologue chain M"
cd ../homolog_model_chainM/ ;
rm -rf emplaced_files_* ; # Clean up before new run
phenix.voyager.em_placement em_place_chainMhomolog.phil > em_placement.log ;
rm voyager_strategy.log ;

echo "Starting respiratory complex homologue chain L"
cd ../homolog_model_chainL/ ;
rm -rf emplaced_files_* ; # Clean up before new run
phenix.voyager.em_placement em_place_chainLhomolog.phil > em_placement.log ;
rm voyager_strategy.log ;

# Remove data
cd ../../../data/respiratory_complex_emd_12654/ ;
rm *.map* ;
cd ../.. ;

########
# MutS #
########

# Fetch data
cd ./data/MutS_emd_11792/ ;
wget https://files.wwpdb.org/pub/emdb/structures/EMD-11792/map/emd_11792.map.gz;
gunzip emd_11792.map.gz;
wget https://files.wwpdb.org/pub/emdb/structures/EMD-11792/other/emd_11792_half_map_1.map.gz;
gunzip emd_11792_half_map_1.map.gz;
wget https://files.wwpdb.org/pub/emdb/structures/EMD-11792/other/emd_11792_half_map_2.map.gz;
gunzip emd_11792_half_map_2.map.gz;

# Test crystal structure model of N-terminal domain
echo "Starting MutS N-terminal domain model"
cd ../../runs/MutS_emd_11792/Ndomain_model/ ;
rm -rf emplaced_files_* ; # Clean up before new run
phenix.voyager.em_placement emplace_6i5f_Nterm.phil > em_placement.log ;
rm voyager_strategy.log ;

# Test crystal structure model of C-terminal domain
echo "Starting MutS C-terminal domain model"
cd ../../runs/MutS_emd_11792/Cdomain_model/ ;
rm -rf emplaced_files_* ; # Clean up before new run
phenix.voyager.em_placement emplace_6i5f_Cterm.phil > em_placement.log ;
rm voyager_strategy.log ;

# Remove data
cd ../../../data/MutS_emd_11792/ ;
rm *.map* ;
cd ../.. ;

#########################
# CFTR deltaF508 mutant #
#########################

# Fetch data
cd ./data/CFTR_dF508_emd_28172/ ;
wget https://files.wwpdb.org/pub/emdb/structures/EMD-28172/map/emd_28172.map.gz;
gunzip emd_28172.map.gz;
wget https://files.wwpdb.org/pub/emdb/structures/EMD-28172/other/emd_28172_half_map_2.map.gz;
gunzip emd_28172_half_map_2.map.gz;
wget https://files.wwpdb.org/pub/emdb/structures/EMD-28172/other/emd_28172_half_map_1.map.gz;
gunzip emd_28172_half_map_1.map.gz;

# Use domain1 of AlphaFold model
echo "Starting CFTR AF domain 1 model"
cd ../../runs/CFTR_dF508_emd_28172/AF_domain1/ ;
rm -rf emplaced_files_* ; # Clean up before new run
phenix.voyager.em_placement em_place_AF_domain1.phil > em_placement.log ;
rm voyager_strategy.log ;

# Use domain2 of AlphaFold model
echo "Starting CFTR AF domain 2 model"
cd ../AF_domain2/ ;
rm -rf emplaced_files_* ; # Clean up before new run
phenix.voyager.em_placement em_place_AF_domain2.phil > em_placement.log ;
rm voyager_strategy.log ;

# Use domain2 of AlphaFold model
echo "Starting CFTR AF domain 3 model"
cd ../AF_domain3/ ;
rm -rf emplaced_files_* ; # Clean up before new run
phenix.voyager.em_placement em_place_AF_domain3.phil > em_placement.log ;
rm voyager_strategy.log ;

# Remove data
cd ../../../data/CFTR_dF508_emd_28172/ ;
rm *.map* ;
cd ../.. ;

###############
# GET3 closed #
###############

# Fetch data
cd ./data/GET3_closed_emd_25375/ ;
wget https://files.wwpdb.org/pub/emdb/structures/EMD-25375/map/emd_25375.map.gz;
gunzip emd_25375.map.gz;
wget https://files.wwpdb.org/pub/emdb/structures/EMD-25375/other/emd_25375_half_map_1.map.gz;
gunzip emd_25375_half_map_1.map.gz;
wget https://files.wwpdb.org/pub/emdb/structures/EMD-25375/other/emd_25375_half_map_2.map.gz;
gunzip emd_25375_half_map_2.map.gz;

# Test with homologue model of GET3
echo "Starting GET3 homologue model"
cd ../../runs/GET3_closed_emd_25375/homolog_model/ ;
rm -rf emplaced_files_* ; # Clean up before new run
phenix.voyager.em_placement emplace_7spzA.phil > em_placement.log ;
rm voyager_strategy.log ;

# Remove data
cd ../../../data/GET3_closed_emd_25375/ ;
rm *.map* ;
cd ../.. ;

